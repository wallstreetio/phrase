
/**
 *
 * WSIOStrtotime
 *
 * This library is a work in progress.
 * Right now, it's designed to read strings like '6am' and `10:35pm`.
 *
 */

;( function( root, factory ) {
  if ( typeof define === 'function' && define.amd ) {
    define( [], factory );
  } else if ( typeof exports === 'object' ) {
    module.exports = factory( require('./wsio-phrase.js') );
  } else {
    root.WSIOStrtotime = factory( root.WSIOPhrase );
  }
}( this, function( WSIOPhrase ) {

  var WSIOStrtotime;

  WSIOStrtotime = new WSIOPhrase( {
    'pm': function( payload ) {
      var hour;

      hour = payload.getHours();

      if ( hour < 12 ) {
        payload.setHours( hour + 12 );
        return payload;
      }

      return payload;
    }
  } );

  //WSIOStrtotime.config = config;
  WSIOStrtotime.getPayload = getHoursAndMinutes;
  WSIOStrtotime.getModifiers = getPostMeridiemFlag;

  return WSIOStrtotime;

  // function config( payload ) {
  //   return payload;
  // }

  /**
   *
   * Returns the payload.
   * Matches `6` and `6:35`.
   *
   * @param  {string} phrase
   * @return {mixed}        the payload
   *
   */
  function getHoursAndMinutes( phrase ) {
    var payload, dt;

    dt = new Date;

    dt.setMilliseconds( 0 );
    dt.setSeconds( 0 );

    payload = phrase.match( /(\d{1,})\:?(\d{1,})?/gi );

    if ( payload.length ) {
      payload = payload[ 0 ].split( ':' );
    }

    switch ( payload.length ) {
      case 1:
        dt.setMinutes( 0 );
        dt.setHours( +payload[ 0 ] );
        break;

      case 2:
        dt.setMinutes( +payload[ 1 ] );
        dt.setHours( +payload[ 0 ] );
        break;

      default:
        dt.setMinutes( 0 );
        dt.setHours( 0 );
        break;
    }

    return dt;
  }

  /**
   *
   * Removes the payload and then chunks the rest of the string
   * into an array of keywords.
   *
   * Please note that the order of the modifiers determines the
   * order in which they will be chained together.
   *
   * @param  {string} phrase
   * @return {array}        an array of modifier keywords
   *
   */
  function getPostMeridiemFlag( phrase ) {
    var payload;

    payload = phrase.match( /(\d{1,})\:?(\d{1,})?/gi );

    if ( payload.length ) {
      return phrase
        // @example `6pm` -> `pm`
        .replace( payload[ 0 ], '' )

        // @note normalize spaces
        .replace( /\s\s+/g, ' ' )

        // @example [ 'pm' ]
        .split( ' ' );
    }

    return [];
  }

} ) );


