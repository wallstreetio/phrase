
describe( 'WSIOPhrase', function(){


  it( 'should get the default payload', function() {
    var phrase, results;

    phrase = new WSIOPhrase;

    results = phrase.compose( 'hello' );

    expect( results ).toBe( 'hello' );
  } );

  it( 'should get an overloaded payload', function() {
    var testPhrase, phrase, results;

    testPhrase = '6pm';
    phrase = new WSIOPhrase;

    phrase.getPayload = function( phrase ) {
      return phrase.match( /\d{1,}/gi )[ 0 ];
    };

    results = phrase.compose( '12pm' );

    expect( results ).toBe( '12' );
  } );

  it( 'should apply modifiers to payload', function() {
    var testPhrase, phrase, results;

    testPhrase = '6pm';

    phrase = new WSIOPhrase( {
      'pm': function( payload ) {
        return ( payload + ' post meridiem' );
      }
    } );

    phrase.getPayload = function( phrase ) {
      return phrase.match( /\d{1,}/gi )[ 0 ];
    };

    phrase.getModifiers = function( phrase ) {
      return phrase
        // `6pm` -> `pm`
        .replace( this.getPayload( phrase ), '' )
        // [ 'pm' ]
        .split( ' ' );
    };

    results = phrase.compose( testPhrase );

    expect( results ).toBe( '6 post meridiem' );
  } );

  it( 'should identify the overloaded modifiers', function() {
    var testPhrase, phrase, results;

    testPhrase = 'tomorrow 6pm';

    phrase = new WSIOPhrase( {
      'tomorrow': function( payload ) {
        return ( 'tomorrow @ ' + payload );
      },
      'pm': function( payload ) {
        return ( payload + ' post meridiem' );
      }
    } );

    phrase.getPayload = function( phrase ) {
      return '6';
    };

    phrase.getModifiers = function( phrase ) {
      return phrase
        // @example `6pm` -> `pm`
        .replace( this.getPayload( phrase ), '' )

        // @note normalize spaces
        .replace( /\s\s+/g, ' ' )

        // @example [ 'tomorrow', 'pm' ]
        .split( ' ' )

        // @example [ 'pm', 'tomorrow' ]
        .reverse();
    };

    results = phrase.getModifiers( testPhrase );

    expect( results.length ).toBe( 2 );
    expect( results[ 0 ] ).toEqual( 'pm' );
    expect( results[ 1 ] ).toEqual( 'tomorrow' );
  } );

  it( 'should map the payload through every modifier', function() {
    var testPhrase, phrase, results;

    testPhrase = 'tomorrow 6pm';

    phrase = new WSIOPhrase( {
      'tomorrow': function( payload ) {
        return ( 'tomorrow @ ' + payload );
      },
      'pm': function( payload ) {
        return ( payload + ' post meridiem' );
      }
    } );

    phrase.getPayload = function( phrase ) {
      return '6';
    };

    phrase.getModifiers = function( phrase ) {
      return phrase
        // @example `6pm` -> `pm`
        .replace( this.getPayload( phrase ), '' )

        // @note normalize spaces
        .replace( /\s\s+/g, ' ' )

        // @example [ 'tomorrow', 'pm' ]
        .split( ' ' )

        // @example [ 'pm', 'tomorrow' ]
        .reverse();
    };

    results = phrase.compose( testPhrase );

    expect( results ).toBe( 'tomorrow @ 6 post meridiem' );

  } );

} );
