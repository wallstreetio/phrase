
describe( 'WSIOStrtotime', function(){

  it( 'should handle basic time value', function() {
    var result, testKey;

    result = WSIOStrtotime.compose( '6am' );

    testKey = new Date;
    testKey.setMilliseconds( 0 );
    testKey.setSeconds( 0 );
    testKey.setMinutes( 0 );
    testKey.setHours( 6 );

    expect( +result ).toBe( +testKey );
  } );

  it( 'should handle hours and minutes', function() {
    var result, testKey;

    result = WSIOStrtotime.compose( '6:35am' );

    testKey = new Date;
    testKey.setMilliseconds( 0 );
    testKey.setSeconds( 0 );
    testKey.setMinutes( 35 );
    testKey.setHours( 6 );

    expect( +result ).toBe( +testKey );
  } );

  it( 'should handle pm keyword', function() {
    var result, testKey;

    result = WSIOStrtotime.compose( '6:35pm' );

    testKey = new Date;
    testKey.setMilliseconds( 0 );
    testKey.setSeconds( 0 );
    testKey.setMinutes( 35 );
    testKey.setHours( 18 );

    expect( +result ).toBe( +testKey );
  } );

} );
