var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var Server = require('karma').Server;

/**
 * Configuration
 */
var config = {
  production: !!plugins.util.env.production,
  port: plugins.util.env.port || 8000,
  host: plugins.util.env.host || 'localhost',
  paths: {
    src: 'lib/',
    build: 'dist/'
  }
};

/**
 * Build Tasks
 */
gulp.task('build', getTask('build'));

/**
 * Watchers
 */
gulp.task('watch', function() {

  gulp.watch(config.paths.src + '/*.js', {
    interval: 2000
  }, ['build']);

});

/**
 * Run test once and exit
 */
gulp.task('test', function (done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, function(){
    done();
  }).start();
});

/**
 * Default Task
 * @run sass, home-scripts, app-scripts
 */
gulp.task('default', ['build']);

/**
 * Retrieve a given task file by name.
 * @param string
 * @return function
 */
function getTask(task) {
  return require('./tasks/' + task)(gulp, plugins, config);
}
