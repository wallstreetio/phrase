module.exports = function(gulp, plugins, config) {
  var scripts = [
    config.paths.src + 'wsio-phrase.js',
    config.paths.src + 'wsio-strtotime.js',
  ];

  return function() {
    var movePhrase, buildStrtotime;

    movePhrase = gulp
      .src([
        config.paths.src + 'wsio-phrase.js'
      ])
      .pipe(config.production ? plugins.uglify() : plugins.util.noop())
      .pipe(gulp.dest('dist'));

    buildStrtotime = gulp
      .src([
        config.paths.src + 'wsio-strtotime.js'
      ])
      .pipe(config.production ? plugins.uglify() : plugins.util.noop())
      .pipe(gulp.dest('dist'));

    // buildStrtotime = gulp.src(scripts)
    //   .pipe(plugins.concat('wsio-strtotime.js'))
    //   .pipe(config.production ? plugins.uglify() : plugins.util.noop())
    //   .pipe(gulp.dest('dist'));

    return Promise.all( [ movePhrase, buildStrtotime ] );
  };
};
