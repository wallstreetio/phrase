
/**
 *
 * WSIOPhrase
 *
 * A tiny minishell engine.
 *
 * @return {[type]} [description]
 */

;( function( root, factory ) {
  if ( typeof define === 'function' && define.amd ) {
    define( [], factory );
  } else if ( typeof exports === 'object' ) {
    module.exports = factory();
  } else {
    root.WSIOPhrase = factory();
  }
}( this, function() {

  WSIOPhrase.prototype.compose = compose;
  WSIOPhrase.prototype.getPayload = getPayload;
  WSIOPhrase.prototype.getModifiers = getModifiers;

  return WSIOPhrase;

  function WSIOPhrase( dictionary ) {
    this.dictionary = ( dictionary || {} );
  }

  /**
   * Sends the phrase payload through any transformers represented
   * by the phrase modifiers.
   *
   * More complex phrases can be easily accomplished by overloading
   * the getPayload and getModifiers.
   *
   * @param  {string} phrase
   * @return {mixed}        The composed result of the transformations.
   */
  function compose( phrase ) {
    var payload, modifiers, next;

    payload = this.getPayload( phrase );

    if ( this.config ) {
      payload = this.config( payload );
    }

    modifiers = this.getModifiers( phrase );

    while ( next = modifiers.shift() ) {
      if ( typeof this.dictionary[ next ] === 'function' ) {
        payload = this.dictionary[ next ]( payload );
      }
    }

    return payload;
  }

  /**
   * Interprets a phrase and returns the desired payload.
   *
   * @param  {string} phrase
   * @return {array}        an array of payload keywords
   */
  function getPayload( phrase ) {
    return phrase;
  }

  /**
   * Interprets a phrase and returns everything else in the phrase.
   *
   * Please note that the order of the modifiers determines the
   * order in which they will be chained together.
   *
   * @param  {string} phrase
   * @return {array}        an array of modifier keywords
   */
  function getModifiers( phrase ) {
    return phrase.split( this.getPayload( phrase ) );
  }

} ) );


